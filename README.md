dxarts462 : Read Me
========================
_DXARTS 462 (Spring 2020): Introduction to digital sound processing._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts462a_sp20.quark).

Start by reviewing the Quark installation instructions
[found here](https://github.com/supercollider-quarks/quarks#installing). See
also [Using Quarks](http://doc.sccode.org/Guides/UsingQuarks.html).

With [git](https://git-scm.com/) installed, you can easily install the
[DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts462a_sp20.quark)
directly by running the following line of code in SuperCollider:

    Quarks.install("https://gitlab.com/dxarts/classes/dxarts462a_sp20.quark");



Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts462a_sp20.quark/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 0.1.9

* Update: add 09a & 09b pages.

Version 0.1.8

* Update: add 08a, 08b, 08c, 08d pages.

Version 0.1.7

* Update: add 06a, 06b, 06c, 06d pages.
* Refactor: fix typo 05b example.

Version 0.1.6

* Update: add 05a, 05b pages.

Version 0.1.5

* Refactor: 04a, include Signal.windowedSinc as LP prototype

Version 0.1.4

* Update: add 04a, 04b pages.

Version 0.1.3

* Update: add 03 page.

Version 0.1.2

* Refactor: fix broken link (02).

Version 0.1.1

* Update: add 02 page.

Version 0.1.0

* First Public Release.


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2012-2020.

Department of Digital Arts and Experimental Media (DXARTS)  
University of Washington

&nbsp;


Contributors
------------

Version pre-release
*  Joseph Anderson (@joslloand)
*  Juan Pampin (@jpampin)
*  Joshua Parmenter (@joshpar)
*  Daniel Peterson (@dmartinp)
*  Stelios Manousakis (@Stylianos91)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts462a_sp20.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
